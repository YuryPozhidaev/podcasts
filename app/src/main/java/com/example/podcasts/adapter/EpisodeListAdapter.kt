package com.example.podcasts.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.podcasts.R
import com.example.podcasts.utils.HtmlUtils
import com.example.podcasts.viewmodel.EpisodeViewData
import java.text.DateFormat
import java.util.*

interface EpisodeListAdapterListener {
    fun onSelectedEpisode(episodeViewData: EpisodeViewData)
}

class EpisodeListAdapter(
    private var episodeViewList: List<EpisodeViewData>?,
    private val episodeListAdapterListener: EpisodeListAdapterListener
) : RecyclerView.Adapter<EpisodeListAdapter.ViewHolder>() {

    class ViewHolder(
        v: View,
        val episodeListAdapterListener: EpisodeListAdapterListener
    ) : RecyclerView.ViewHolder(v) {

        var episodeViewData: EpisodeViewData? = null
        val titleTextView: TextView = v.findViewById(R.id.titleView)
        val descTextView: TextView = v.findViewById(R.id.descView)
        val durationTextView: TextView = v.findViewById(R.id.durationView)
        val releaseDateTextView: TextView = v.findViewById(R.id.releaseDateView)

        init { v.setOnClickListener {
            episodeViewData?.let {
                episodeListAdapterListener.onSelectedEpisode(it) }
        } }
    }

    fun setViewData(episodeList: List<EpisodeViewData>) {
        episodeViewList = episodeList
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            : ViewHolder =
        ViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.episode_item, parent, false),
            episodeListAdapterListener
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val episodeViewList = episodeViewList ?: return
        val episodeView = episodeViewList[position]
        holder.episodeViewData = episodeView
        holder.titleTextView.text = episodeView.title
        holder.descTextView.text = HtmlUtils.htmlToSpannable(episodeView.description)
        holder.durationTextView.text = episodeView.duration
        holder.releaseDateTextView.text = dateToShortDate(episodeView.releaseDate)
    }

    private fun dateToShortDate(date: Date?): String {
        val outputFormat = DateFormat.getDateInstance(
            DateFormat.SHORT, Locale.getDefault()
        )
        return outputFormat.format(date)
    }

    override fun getItemCount(): Int {
        return episodeViewList?.size ?: 0
    }
}