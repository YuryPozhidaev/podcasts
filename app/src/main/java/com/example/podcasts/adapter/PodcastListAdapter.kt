package com.example.podcasts.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.podcasts.R
import com.example.podcasts.viewmodel.PodcastSummaryViewData

class PodcastListAdapter(
    private var podcastSummaryViewList: List<PodcastSummaryViewData>?,
    private val podcastListAdapterListener: PodcastListAdapterListener,
    private val parentActivity: Activity
) : RecyclerView.Adapter<PodcastListAdapter.ViewHolder>() {

    interface PodcastListAdapterListener {
        fun onShowDetails(podcastSummaryViewData: PodcastSummaryViewData)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PodcastListAdapter.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.search_item, parent, false),
            podcastListAdapterListener
        )
    }

    override fun getItemCount(): Int = podcastSummaryViewList?.size ?: 0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val searchViewList = podcastSummaryViewList
        val searchView = searchViewList?.get(position)
        holder.podcastSummaryViewData = searchView ?: return
        holder.nameTextView.text = searchView.name
        holder.lastUpdatedTextView.text = searchView.lastUpdated

        Glide.with(parentActivity)
            .load(searchView.imageUrl)
            .into(holder.podcastImageView)
    }

    fun setSearchData(podcastSummaryViewData: List<PodcastSummaryViewData>) {
        podcastSummaryViewList = podcastSummaryViewData
        this.notifyDataSetChanged()
    }

    inner class ViewHolder(
        view: View, private val podcastListAdapterListener:
        PodcastListAdapterListener
    ) : RecyclerView.ViewHolder(view) {

        var podcastSummaryViewData: PodcastSummaryViewData? = null
        val nameTextView: TextView =
            view.findViewById(R.id.podcastNameTextView)
        val lastUpdatedTextView: TextView =
            view.findViewById(R.id.podcastLastUpdatedTextView)
        val podcastImageView: ImageView =
            view.findViewById(R.id.podcastImage)

        init {
            view.setOnClickListener {
                podcastSummaryViewData?.let { podcast ->
                    podcastListAdapterListener.onShowDetails(podcast)
                }
            }
        }

    }
}