package com.example.podcasts.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.example.podcasts.repository.ItunesRepository
import com.example.podcasts.service.PodcastResponse
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class SearchViewModel(application: Application) : AndroidViewModel(application) {

    var iTunesRpository: ItunesRepository? = null

    private fun itunesPodcastToPodcastSummaryView(itunesPodcast: PodcastResponse.ItunesPodcast): PodcastSummaryViewData =
        PodcastSummaryViewData(
            itunesPodcast.collectionCensoredName,
            jsonDateToFormatDate(itunesPodcast.releaseDate),
            itunesPodcast.artworkUrl100,
            itunesPodcast.feedUrl
        )


    private fun jsonDateToFormatDate(jsonDate: String): String =
        SimpleDateFormat
            .getDateInstance(DateFormat.SHORT, Locale.getDefault())
            .format(SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(jsonDate))

    fun searchPodcasts(term: String, callback: (List<PodcastSummaryViewData>) -> Unit) {
        iTunesRpository?.searchByTerm(term) { results ->
            val searchViews = results?.map { podcast ->
                itunesPodcastToPodcastSummaryView(podcast)
            }
            searchViews?.let { callback(it) }
        }
    }
}

data class PodcastSummaryViewData(
    val name: String,
    val lastUpdated: String,
    val imageUrl: String,
    val feedUrl: String
)
