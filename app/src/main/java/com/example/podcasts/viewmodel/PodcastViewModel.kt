package com.example.podcasts.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.podcasts.model.Episode
import com.example.podcasts.model.Podcast
import com.example.podcasts.repository.PodcastRepository
import java.text.DateFormat
import java.util.*

class PodcastViewModel(application: Application) : AndroidViewModel(application) {

    var podcastRepository: PodcastRepository? = null
    var activePodcastViewData: PodcastViewData? = null
    var livePodcastData: LiveData<List<PodcastSummaryViewData>>? = null
    var activeEpisodeViewData: EpisodeViewData? = null

    private var activePodcast: Podcast? = null

    fun setActivePodcast(
        feedUrl: String,
        callback: (PodcastSummaryViewData?) -> Unit
    ) {
        podcastRepository?.getPodcast(feedUrl) { podcast ->
            if (podcast == null) {
                callback(null)
            } else {
                activePodcastViewData = podcastToPodcastView(podcast)
                activePodcast = podcast
                callback(podcastToSummaryView(podcast))
            }
        }
    }

    fun getPodcast(
        podcastSummaryViewData: PodcastSummaryViewData,
        callback: (PodcastViewData?) -> Unit
    ) {
        podcastRepository?.getPodcast(podcastSummaryViewData.feedUrl) { podcast ->
            podcast.let {
                it?.feedTitle = podcastSummaryViewData.name
                it?.imageUrl = podcastSummaryViewData.imageUrl
                activePodcastViewData = it?.let { data -> podcastToPodcastView(data) }
                activePodcast = it
                callback(activePodcastViewData)
            }
        }
    }

    fun getPodcasts(): LiveData<List<PodcastSummaryViewData>>? {
        val repo = podcastRepository ?: return null
        if (livePodcastData == null) {
            val liveData = repo.getAll()
            livePodcastData = Transformations.map(liveData) { podcastList ->
                podcastList.map { podcast ->
                    podcastToSummaryView(podcast)
                }
            }
        }
        return livePodcastData
    }

    fun deleteActivePodcast() {
        activePodcast?.let {
            podcastRepository?.delete(it)
        }
    }

    private fun dateToShortDate(date: Date): String =
        DateFormat
            .getDateInstance(DateFormat.SHORT, Locale.getDefault())
            .format(date)

    private fun podcastToSummaryView(podcast: Podcast): PodcastSummaryViewData =
        PodcastSummaryViewData(
            podcast.feedTitle,
            dateToShortDate(podcast.lastUpdated),
            podcast.imageUrl,
            podcast.feedUrl
        )

    fun saveActivePodcast() {
        activePodcast?.let {
            it.episodes = it.episodes.drop(1)
            podcastRepository?.save(it)
        }
    }

    private fun episodesToEpisodesView(episodes: List<Episode>)
            : List<EpisodeViewData> =
        episodes.map { episode ->
            EpisodeViewData(
                episode.guid,
                episode.title,
                episode.description,
                episode.mediaUrl,
                episode.releaseDate,
                episode.duration,
                episode.mimeType.startsWith("video")
            )
        }

    private fun podcastToPodcastView(podcast: Podcast): PodcastViewData =
        PodcastViewData(
            podcast.id != null,
            podcast.feedTitle,
            podcast.feedUrl,
            podcast.feedDesc,
            podcast.imageUrl,
            episodesToEpisodesView(podcast.episodes)
        )

}

data class PodcastViewData(
    var subscribed: Boolean = false,
    val feedTitle: String,
    val feedUrl: String,
    val feedDesc: String,
    val imageUrl: String,
    val episodes: List<EpisodeViewData>
)

data class EpisodeViewData(
    val guid: String,
    val title: String,
    val description: String,
    val mediaUrl: String,
    val releaseDate: Date?,
    val duration: String,
    var isVideo: Boolean = false
)