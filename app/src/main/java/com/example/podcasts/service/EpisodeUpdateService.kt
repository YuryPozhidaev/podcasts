package com.example.podcasts.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.example.podcasts.R
import com.example.podcasts.db.PodPlayDatabase
import com.example.podcasts.repository.PodcastRepository
import com.example.podcasts.ui.MainActivity
import com.firebase.jobdispatcher.JobParameters
import com.firebase.jobdispatcher.JobService
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class EpisodeUpdateService : JobService() {

    companion object {
        const val EPISODE_CHANNEL_ID = "podplay_episode_channel"
        const val EXTRA_FEED_URL = "PodcastFeedUrl"
    }

    override fun onStartJob(job: JobParameters): Boolean {
        val podcastRepository =
            PodcastRepository(FeedService.instance, PodPlayDatabase.getInstance(this).podcastDao())
        GlobalScope.launch {
            podcastRepository.updatePodcastEpisodes { podcastUpdates ->
                Log.i("NOTIFY", "NOTIFY")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    createNotificationChannel()
                }
                for (podcastUpdate in podcastUpdates) {
                    displayNotification(podcastUpdate)
                }
                jobFinished(job, false)
            }
        }
        return true
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel() {
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as
                NotificationManager

        if (notificationManager.getNotificationChannel(EPISODE_CHANNEL_ID) == null) {
            val channel = NotificationChannel(
                EPISODE_CHANNEL_ID,
                "Episodes",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun displayNotification(podcastInformation: PodcastRepository.PodcastUpdateInfo) {
        val contentIntent = Intent(this, MainActivity::class.java)
        contentIntent.putExtra(EXTRA_FEED_URL, podcastInformation.feedUrl)
        val pendingContentIntent = PendingIntent.getActivity(
            this,
            0,
            contentIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        val notification = NotificationCompat.Builder(this, EPISODE_CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_episode_icon)
            .setContentTitle(getString(R.string.episode_notification_title))
            .setContentText(
                getString(
                    R.string.episode_notification_text,
                    podcastInformation.newCount,
                    podcastInformation.name
                )
            )
            .setNumber(podcastInformation.newCount)
            .setAutoCancel(true)
            .setContentIntent(pendingContentIntent)
            .build()

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(podcastInformation.name, 0, notification)
    }

    override fun onStopJob(job: JobParameters): Boolean {
        return true
    }
}