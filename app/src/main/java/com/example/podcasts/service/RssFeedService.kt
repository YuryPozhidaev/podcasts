package com.example.podcasts.service

import okhttp3.*
import org.w3c.dom.Node
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import javax.xml.parsers.DocumentBuilderFactory

interface FeedService {
    fun getFeed(xmlFileUrl: String, callback: (RssFeedResponse?) -> Unit)

    companion object {
        val instance: FeedService by lazy {
            RssFeedService()
        }
    }
}

class RssFeedService : FeedService {

    override fun getFeed(xmlFileUrl: String, callback: (RssFeedResponse?) -> Unit) {
        OkHttpClient()
            .newCall(
                Request
                    .Builder()
                    .url(xmlFileUrl)
                    .build()
            )
            .enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    callback(null)
                }

                override fun onResponse(call: Call, response: Response) {
                    if (response.isSuccessful) {
                        response.body()?.let { responseBody ->
                            val doc = DocumentBuilderFactory
                                .newInstance()
                                .newDocumentBuilder()
                                .parse(responseBody.byteStream())

                            val resFeedResponse = RssFeedResponse(episodes = mutableListOf())
                            domToRssFeedResponse(doc, resFeedResponse)
                            callback(resFeedResponse)
                            println(resFeedResponse)
                            return
                        }
                    }
                    callback(null)
                }
            })
    }

    private fun xmlDateToDate(date: String): Date =
        SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z").parse(date)

    private fun domToRssFeedResponse(node: Node, rssFeedResponse: RssFeedResponse) {

        if (node.nodeType == Node.ELEMENT_NODE) {
            if (node.parentNode.nodeName == "item" && node.parentNode.parentNode?.nodeName ?: "" == "channel") {
                val currentItem = rssFeedResponse.episodes?.last()
                when (node.nodeName) {
                    "title" -> currentItem?.title = node.textContent
                    "description" -> currentItem?.description = node.textContent
                    "itunes:duration" -> currentItem?.duration = node.textContent
                    "guid" -> currentItem?.guid = node.textContent
                    "pubDate" -> currentItem?.pubDate = node.textContent
                    "link" -> currentItem?.link = node.textContent
                    "enclosure" -> {
                        currentItem?.url = node.attributes.getNamedItem("url").textContent
                        currentItem?.type = node.attributes.getNamedItem("type").textContent
                    }
                }
            }

            if (node.parentNode.nodeName == "channel") {
                when (node.nodeName) {
                    "title" -> rssFeedResponse.title = node.textContent
                    "description" -> rssFeedResponse.description = node.textContent
                    "itunes:summary" -> rssFeedResponse.summary = node.textContent
                    "item" -> rssFeedResponse.episodes?.add(RssFeedResponse.EpisodeResponse())
                    "pubDate" -> rssFeedResponse.lastUpdated = xmlDateToDate(node.textContent)
                }
            }
        }

        for (i in 0 until node.childNodes.length) {
            val childNode = node.childNodes.item(i)
            domToRssFeedResponse(childNode, rssFeedResponse)
        }
    }
}