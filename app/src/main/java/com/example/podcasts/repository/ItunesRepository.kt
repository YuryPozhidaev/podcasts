package com.example.podcasts.repository

import com.example.podcasts.service.ItunesService
import com.example.podcasts.service.PodcastResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ItunesRepository(private val itunesService: ItunesService) {

    fun searchByTerm(term: String, callBack: (List<PodcastResponse.ItunesPodcast>?) -> Unit) {
         itunesService.searchPodcastByTerm(term).enqueue(object : Callback<PodcastResponse> {
            override fun onFailure(
                call: Call<PodcastResponse>?,
                t: Throwable?
            ) {
                callBack(null)
            }

            override fun onResponse(
                call: Call<PodcastResponse>?,
                response: Response<PodcastResponse>?
            ) {
                val body = response?.body()
                callBack(body?.results)
            }
        })
    }
}