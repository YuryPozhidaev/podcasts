package com.example.podcasts.repository

import androidx.lifecycle.LiveData
import com.example.podcasts.db.PodcastDao
import com.example.podcasts.model.Episode
import com.example.podcasts.model.Podcast
import com.example.podcasts.service.FeedService
import com.example.podcasts.service.RssFeedResponse
import com.example.podcasts.viewmodel.PodcastSummaryViewData
import kotlinx.coroutines.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.coroutines.CoroutineContext

class PodcastRepository(
    private val feedService: FeedService,
    private val podcastDao: PodcastDao
) : CoroutineScope {

    private val job = Job()
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private fun rssItemsToEpisodes(episodeResponse: List<RssFeedResponse.EpisodeResponse>) =
        episodeResponse.map {
            Episode(
                it.guid ?: "",
                0,
                it.title ?: "",
                it.description ?: "",
                it.url ?: "",
                it.type ?: "",
                xmlDateToDate(it.pubDate),
                it.duration ?: ""
            )
        }

    private fun xmlDateToDate(date: String?): Date =
        SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z").parse(date)

    private fun rssResponseToPodcast(
        feedUrl: String,
        imageUrl: String,
        rssResponse: RssFeedResponse
    ): Podcast? {

        val items = rssResponse.episodes ?: return null
        val description =
            if (rssResponse.description == "")
                rssResponse.summary
            else
                rssResponse.description

        return Podcast(
            null,
            feedUrl,
            rssResponse.title,
            description,
            imageUrl,
            rssResponse.lastUpdated,
            episodes = rssItemsToEpisodes(items)
        )
    }

    private fun getNewEpisodes(localPodcast: Podcast, callback: (List<Episode>) -> Unit) {
        feedService.getFeed(localPodcast.feedUrl) { response ->
            if (response != null) {
                val remotePodcast =
                    rssResponseToPodcast(localPodcast.feedUrl, localPodcast.imageUrl, response)
                remotePodcast?.let {
                    val localEpisodes = podcastDao.loadEpisodes(localPodcast.id!!)
                    val newEpisodes = remotePodcast.episodes.filter { episode ->
                        localEpisodes.find { episode.guid == it.guid } == null
                    }
                    callback(newEpisodes)
                }
            } else {
                callback(listOf())
            }
        }
    }

    private fun saveNewEpisodes(podcastId: Long, episodes: List<Episode>) {
        GlobalScope.launch {
            for (episode in episodes) {
                episode.podcastId = podcastId
                podcastDao.insertEpisode(episode)
            }
        }
    }

    fun save(podcast: Podcast) {
        GlobalScope.launch {
            val podcastId = podcastDao.insertPodcast(podcast)
            for (episode in podcast.episodes) {
                episode.podcastId = podcastId
                podcastDao.insertEpisode(episode)
            }
        }
    }

    fun getAll(): LiveData<List<Podcast>> = podcastDao.loadPodcasts()

    fun getPodcast(feedUrl: String, callback: (Podcast?) -> Unit) {
        GlobalScope.launch {
            val podcast = podcastDao.loadPodcast(feedUrl)
            if (podcast != null) {
                podcast.id?.let {
                    podcast.episodes = podcastDao.loadEpisodes(it)
                    GlobalScope.launch(Dispatchers.Main) {
                        callback(podcast)
                    }
                }
            } else {
                feedService.getFeed(feedUrl) { feedResponse ->
                    if (feedResponse == null) {
                        GlobalScope.launch(Dispatchers.Main) {
                            callback(null)
                        }
                    } else {
                        val podcast = rssResponseToPodcast(feedUrl, "", feedResponse)
                        GlobalScope.launch(Dispatchers.Main) {
                            callback(podcast)
                        }
                    }
                }
            }
        }
    }

    fun delete(podcast: Podcast) {
        GlobalScope.launch {
            podcastDao.deletePodcast(podcast)
        }
    }

    fun updatePodcastEpisodes(callback: (List<PodcastUpdateInfo>) -> Unit) {
        val updatedPodcasts: MutableList<PodcastUpdateInfo> = mutableListOf()
        var processCount = podcastDao.loadPodcastsStatic().count()
        for (podcast in podcastDao.loadPodcastsStatic()) {
            getNewEpisodes(podcast) { newEpisodes ->
                if (newEpisodes.count() > 0) {
                    saveNewEpisodes(podcast.id!!, newEpisodes)
                    updatedPodcasts.add(
                        PodcastUpdateInfo(
                            podcast.feedUrl,
                            podcast.feedTitle,
                            newEpisodes.count()
                        )
                    )
                }
                processCount--
                if (processCount == 0) {
                    callback(updatedPodcasts)
                }
            }
        }
    }

    class PodcastUpdateInfo(val feedUrl: String, val name: String, val newCount: Int)
}