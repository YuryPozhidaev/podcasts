package com.example.podcasts.db

import android.content.Context
import androidx.room.*
import com.example.podcasts.model.Episode
import com.example.podcasts.model.Podcast
import java.util.*

@Database(
    entities = [Podcast::class, Episode::class],
    version = 1
)
@TypeConverters(Converters::class)
abstract class PodPlayDatabase : RoomDatabase() {

    abstract fun podcastDao(): PodcastDao

    companion object {

        private var instance: PodPlayDatabase? = null

        fun getInstance(context: Context): PodPlayDatabase {
            if (instance == null) {

                instance = Room.databaseBuilder(
                    context.applicationContext,
                    PodPlayDatabase::class.java,
                    "PodPlayer2"
                ).build()
            }
            return instance as PodPlayDatabase
        }
    }
}

class Converters {

    @TypeConverter
    fun fromTimestamp(value: Long): Date = Date(value)

    @TypeConverter
    fun toTimestamp(date: Date): Long = date.time
}