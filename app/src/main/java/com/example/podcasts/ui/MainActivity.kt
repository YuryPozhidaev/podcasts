package com.example.podcasts.ui

import android.app.AlertDialog
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.podcasts.R
import com.example.podcasts.adapter.PodcastListAdapter
import com.example.podcasts.db.PodPlayDatabase
import com.example.podcasts.repository.ItunesRepository
import com.example.podcasts.repository.PodcastRepository
import com.example.podcasts.service.EpisodeUpdateService
import com.example.podcasts.service.FeedService
import com.example.podcasts.service.ItunesService
import com.example.podcasts.viewmodel.EpisodeViewData
import com.example.podcasts.viewmodel.PodcastSummaryViewData
import com.example.podcasts.viewmodel.PodcastViewModel
import com.example.podcasts.viewmodel.SearchViewModel
import com.firebase.jobdispatcher.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity :
    AppCompatActivity(),
    PodcastListAdapter.PodcastListAdapterListener,
    OnPodcastDetailsListener {

    private val TAG_EPISODE_UPDATE_JOB = "com.examples.podcasts.episodes"

    private lateinit var searchViewModel: SearchViewModel
    private lateinit var podcastListAdapter: PodcastListAdapter
    private lateinit var searchMenuItem: MenuItem
    private lateinit var podcastViewModel: PodcastViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupToolbar()
        setupViewModels()
        updateControls()
        setupPodcastListView()
        handleIntent(intent)
        addBackStackListener()
        scheduleJobs()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)

        searchMenuItem = menu.findItem(R.id.search_item)
        searchMenuItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(p0: MenuItem?): Boolean {
                return true
            }

            override fun onMenuItemActionCollapse(p0: MenuItem?): Boolean {
                showSubscribedPodcasts()
                return true
            }
        })

        val searchView = searchMenuItem.actionView as SearchView
        val searchMenu = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        searchView.setSearchableInfo(searchMenu.getSearchableInfo(componentName))

        if (supportFragmentManager.backStackEntryCount > 0) {
            podcastRecyclerView.visibility = View.INVISIBLE
        }

        if (podcastRecyclerView.visibility == View.INVISIBLE) {
            searchMenuItem.isVisible = false
        }

        return true
    }

    private fun createPodcastDetailsFragment(): PodcastDetailsFragment {
        var podcastDetailsFragment =
            supportFragmentManager.findFragmentByTag(TAG_DETAILS_FRAGMENT) as PodcastDetailsFragment?

        if (podcastDetailsFragment == null) {
            podcastDetailsFragment = PodcastDetailsFragment.newInstance()
        }
        return podcastDetailsFragment
    }

    private fun showDetailsFragment() {
        supportFragmentManager
            .beginTransaction()
            .add(R.id.podcastDetailsContainer, createPodcastDetailsFragment(), TAG_DETAILS_FRAGMENT)
            .addToBackStack("DetailsFragment")
            .commit()

        podcastRecyclerView.visibility = View.INVISIBLE
        searchMenuItem.isVisible = false
    }

    private fun scheduleJobs() {
        val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(this))
        val oneHourInSeconds = 60
        val tenMinutesInSeconds = 6
        val episodeUpdateJob = dispatcher.newJobBuilder()
            .setService(EpisodeUpdateService::class.java)
            .setTag(TAG_EPISODE_UPDATE_JOB)
            .setRecurring(true)
            .setTrigger(
                Trigger.executionWindow(
                    oneHourInSeconds,
                    (oneHourInSeconds + tenMinutesInSeconds)
                )
            )
            .setLifetime(Lifetime.FOREVER)
            .setConstraints(Constraint.ON_UNMETERED_NETWORK, Constraint.DEVICE_CHARGING)
            .build()
        dispatcher.mustSchedule(episodeUpdateJob)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setIntent(intent)
        handleIntent(intent)
    }

    private fun setupViewModels() {
        searchViewModel = ViewModelProviders
            .of(this)
            .get(SearchViewModel::class.java)
        searchViewModel.iTunesRpository = ItunesRepository(ItunesService.instance)

        podcastViewModel = ViewModelProviders
            .of(this)
            .get(PodcastViewModel::class.java)

        podcastViewModel.podcastRepository = PodcastRepository(
            FeedService.instance,
            PodPlayDatabase.getInstance(this).podcastDao()
        )
    }

    private fun updateControls() {
        podcastRecyclerView.setHasFixedSize(true)

        val layoutManager = LinearLayoutManager(this)
        podcastRecyclerView.layoutManager = layoutManager
        val dividerItemDecoration = DividerItemDecoration(
            podcastRecyclerView.context,
            layoutManager.orientation
        )
        podcastRecyclerView.addItemDecoration(dividerItemDecoration)
        podcastListAdapter = PodcastListAdapter(null, this, this)
        podcastRecyclerView.adapter = podcastListAdapter

    }

    private fun addBackStackListener() {
        supportFragmentManager.addOnBackStackChangedListener {
            if (supportFragmentManager.backStackEntryCount == 0) {
                podcastRecyclerView.visibility = View.VISIBLE
            }
        }
    }

    private fun showError(message: String) {
        AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton(getString(R.string.ok_button), null)
            .create()
            .show()
    }

    private fun performSearch(term: String) {
        // showProgressBar()
        searchViewModel.searchPodcasts(term) { results ->
            Log.i("TAG", results.toString())
            //  hideProgressBar()
            toolbar.title = getString(R.string.search_results)
            podcastListAdapter.setSearchData(results)
        }
    }

    private fun setupPodcastListView() {
        podcastViewModel.getPodcasts()?.observe(this, Observer {
            if (it != null) {
                showSubscribedPodcasts()
            }
        })
    }

    private fun showSubscribedPodcasts() {
        val podcasts = podcastViewModel.getPodcasts()?.value
        if (podcasts != null) {
            toolbar.title = getString(R.string.subscribed_podcasts)
            podcastListAdapter.setSearchData(podcasts)
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
    }

    private fun handleIntent(intent: Intent) {
        if (intent.action == Intent.ACTION_SEARCH)
            performSearch(intent.getStringExtra(SearchManager.QUERY))

        val podcastFeedUrl = intent.getStringExtra(EpisodeUpdateService.EXTRA_FEED_URL)
        if (podcastFeedUrl != null) {
            Log.i("FEED_URL", podcastFeedUrl)
            podcastViewModel.setActivePodcast(podcastFeedUrl) {
                it?.let { podcastSummaryViewData -> onShowDetails(podcastSummaryViewData) }
            }
        }
    }

    private fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        progressBar.visibility = View.INVISIBLE
    }

    override fun onShowDetails(podcastSummaryViewData: PodcastSummaryViewData) {
        //showProgressBar()
        podcastViewModel.getPodcast(podcastSummaryViewData) { podcastViewData ->
            //hideProgressBar()
            if (podcastViewData != null) {
                showDetailsFragment()
            } else {
                showError("Error loading feed ${podcastSummaryViewData.feedUrl}")
            }
        }
    }

    private fun createEpisodePlayerFragment(): EpisodePlayerFragment {
        var episodePlayerFragment =
            supportFragmentManager.findFragmentByTag(TAG_PLAYER_FRAGMENT) as EpisodePlayerFragment?
        if (episodePlayerFragment == null) {
            episodePlayerFragment = EpisodePlayerFragment.newInstance()
        }
        return episodePlayerFragment
    }

    private fun showPlayerFragment() {
        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.podcastDetailsContainer,
                createEpisodePlayerFragment(),
                TAG_PLAYER_FRAGMENT
            )
            .addToBackStack("PlayerFragment")
            .commit()
        podcastRecyclerView.visibility = View.INVISIBLE
        searchMenuItem.isVisible = false
    }

    companion object {
        private const val TAG_DETAILS_FRAGMENT = "DetailsFragment"
        private const val TAG_PLAYER_FRAGMENT = "PlayerFragment"
    }

    override fun onSubscribe() {
        podcastViewModel.saveActivePodcast()
        supportFragmentManager.popBackStack()
    }

    override fun onUnsubscribe() {
        podcastViewModel.deleteActivePodcast()
        supportFragmentManager.popBackStack()
    }

    override fun onShowEpisodePlayer(episodeViewData: EpisodeViewData) {
        podcastViewModel.activeEpisodeViewData = episodeViewData
        showPlayerFragment()
    }
}
